# Builder.h

The file [Builder.h](Builder.h) contains two small excerpts of code from my [ciCMS](https://github.com/markkorput/ciCMS) repository which is a [Cinder](http://libcinder.org) plugin that I've created that contains exploratory code into separating configuration code from application logic and evolved into a
mechanic that (de-)composes components based on C++ classes at runtime from a json config file.

The two excerpts in [Builder.h](Builder.h) show some technical details on how I tackled the problem of dynamically instantiating (AND cleaning up) any type of C++ object based on dynamically loadable configuration data (json).

For further details I'll refer to the  [README on github](https://github.com/markkorput/ciCMS), for now I'll just mention it's an evolution (probably one step too far) on my efforts in the [JavaLibUiBuilder](https://github.com/fusefactory/JavaLibUiBuilder) package that I created and applied while working at Fuse*Factory to speed up development and iteration while working their java-based touch-screen applications.

Related repositories:
 * [ciCMS](https://github.com/markkorput/ciCMS) - the repo that these excerpts are taken from
 * [JavaLibUiBuilder](https://github.com/fusefactory/JavaLibUiBuilder) - the Java-based predecessor
 * [pycfgr](https://github.com/markkorput/pycfgr) - the python-based sibling

# MicroState

Repository: [MicroState](https://github.com/markkorput/microstate) (note that the repo's README 
is a bit outdated)

MicroState is a Unity package that contains classes and behaviours that do the heavy lifting for creating data-driven application architecture. It lets you easily create State providers and behaviours to manipulate the state attributes or that automatically execute when the state is updated. 

Here is an example of an implementation of the state provider behaviour:
![microstate-state-example](microstate_state.png)

[The code:](https://github.com/markkorput/MicroState/blob/topic/Demos/CustomStateInstance.cs)

```c#

// The Data class; simple class/struct that defines the data properties
[System.Serializable]
public class CustomData
{
  public string Name = "";
  public int Number = 0;
  public bool Approved = false;
}

// The State class; defines accessors and callbacks through string-identifiable virtual attributes.
public class CustomState : Id.IdState<CustomData>
{
  public CustomState() : this(new CustomData()) {}

  public CustomState(CustomData instance) : base(instance)
  {
    base.CreateAttr<string>("Name",
                (state) => state.Name,
                (state, val) => state.Name = val);
        
    base.CreateAttr<int>("Number",
                (state) => state.Number,
                (state, val) => state.Number = val);
        
    base.CreateAttr<bool>("Approved",
                (state) => state.Approved,
                (s, b) => s.Approved = b);
  }
}

// The (Unity) state behaviour that provides a custom Unity Editor out-of-the-box
public class CustomStateInstance : Id.IdStateInstance<CustomData, CustomState>
{
}
```

The following is a StringAttr behaviour that can be used as interface to MicroState attributes (in this case the ```Name``` attribute from the example above), either to update their value, or to execute behaviour when their value changes. In this example a Text element's text property will be updated whenever the ```Name``` attribute in the ```MainState``` changes.

![microstate-attr-example](microstate_stringattr.png)


# "Grabbing" UX in VR with Leap Motion

This example is a small case study in which the above MicroState package was used. It demonstrates data-driven logic in which different behaviours (viewing, highlighting, grabbing, selecting) are kept modular and independent/autonomous by letting them only interact with a central state.

This application uses VR and Leap Motion to let the user interact solely through looking and hand gestures. One of the UX problems to solve was how to let the player select POIs without complete freedom of movement (movement is only possible by looking at and moving predefined positions). Our solution was to spawn a "grabable" placeholder for every seletable POI within the viewport of the player and position those at a comfortable distance of the player.

Screenshot:
![grabbing-screenshot.png](grabbing-screenshot.png)
_POIs (headhpones, head and phone) and their corresponding "grabable" placeholders, closer to the player_


Below is a sequence diagram that documents the (data-driven) "Grabbing interaction". Note how a large part of the interaction logic consists of state notificiations (dotted lines) that are handled by the MicroState package and don't require custom implementation.

[![grabbing seuence diagram](grabbing.svg)](grabbing.svg)


_Summary:_

There's a ```SceneState``` with information on selectable items for which grabbing interaction should be provided;
 * ```Selectable``` behaviours set the corresponding ```in-view``` attribute to ```true``` when the selectable object enters the player's viewport (and to ```false``` when it leaves the viewport).
 * The ```GrabableSpawner``` Spawns a ```Grabable``` object when a Selectable's ```in-view``` attribute turns to ```true```.

There's a ```GrabState``` that forms the single source of truth for the Grabbing interaction
 * The ```GrabableSpawner``` updates Grabable information based on which grabables are spawned (in response to visibility of selectables, see above).
 * ```GrabberSpawner``` updates Grabber attributes based on leap motion input.
 * ```GrabController``` updates the GrabState whenever a grabable is; ```hovered``` (grabber within close distance of grabable), ```grabbed``` (player closes his/her hand while hovering a Grabable) or ```selected``` (grabbed for a certain amount of time)

 Below is a screenshot of the GrabState editor, showing two Grabable items and one Grabber item (the hand of the player reaching for one of the items).
 
 ![GrabState behaviour](grabbing_grabstate.png)

 # Computils

Repository: [https://github.com/markkorput/Computils](https://github.com/markkorput/Computils)

This repository contains code for exploring the use of Compute Shaders in Unity.

![Screenshot1](https://github.com/markkorput/Computils/blob/master/screenshot1.png?raw=true)
![Screenshot2](https://github.com/markkorput/Computils/blob/master/screenshot2.png?raw=true)

Some highlights from the [GravityDemo](https://github.com/markkorput/Computils/tree/master/Demos) (above screenshots);

The [ForcesCollector](https://github.com/markkorput/Computils/blob/master/Scripts/Processors/ForcesCollector.cs) accumulates various forces (each force inherits from the [Force](https://github.com/markkorput/Computils/blob/master/Scripts/Processors/Forces/Force.cs) behaviour) into a forces buffer an applies them either to the positions buffer or a provided velocities buffer.
 * A [GravityForce](https://github.com/markkorput/Computils/blob/master/Scripts/Processors/Forces/GravityForce.cs) pulls particles towards the center ([shader](https://github.com/markkorput/Computils/blob/master/Scripts/Processors/Forces/GravityForceShader.compute))
 * Another [GravityForce](https://github.com/markkorput/Computils/blob/master/Scripts/Processors/Forces/GravityForce.cs) pulls particles towards the satellite
 * A [LinearFalloffAttractorForce](https://github.com/markkorput/Computils/blob/master/Scripts/Processors/Forces/LinearFalloffAttractorForce.cs) with a negative force pushes the particles away when they come too close to the center ([shader](https://github.com/markkorput/Computils/blob/master/Scripts/Processors/Forces/LinearFalloffAttractorShader.compute))
 * The [SingleColorRenderer](https://github.com/markkorput/Computils/blob/master/Scripts/Renderers/SingleColorRenderer.cs) renders the particles ([shader](https://github.com/markkorput/Computils/blob/master/Scripts/Renderers/SingleColorDoubleSidedShader.shader))


# XSD populator (Ruby)

This is a pretty old one, created while I was working for [Fuga](https://fuga.com/). The Fuga platform maintains many different distribution channels to Digital Service Providers (DSPs) like iTunes, Amazon, Spotify, Youtube, etc. etc. Maintaining these channels generally involves patching giant XML metadata generators that have to comply with the DSP's custom specs. 

Luckily the DDEX standard provides some consistency across the industry, but even if DSPs used thise standard for metadata exchange, each still has a lot of freedom in how to implement the spec.

Traditional XML libraries like [XmlMarkup](https://www.rubydoc.info/gems/activesupport/2.3.17/Builder/XmlMarkup) provide a linear approach to generating XML structures; one-by-one you add field to the hierarchy. This works fine for generating individual XML documents, but with the amount of channels we had to support, standardization was essential. Code resuse for traditional XML builders is pretty hard though, we'd run into situations where customization for one field in the XML hierarchy of a shared XML generator would require either the entire sub-hierarchy to be duplicated, or an growing abundance of flags and if/else statements that obfuscate the structure of the XML in the code.

My XsdPopulator approach flipped this process on its head; instead of XML generators that define both the structure and the content of the produced XML, the XsdPopulator takes an XSD (Xml Schema Definition) as input (XSD schemas are generally used for validating XML data) and based on that structure, pulls data and information from data providers to produce XML. This way the produced XML by definition meets the XSD spec (assuming the XsdPopulator is bug-free) and implementation can focus more on the data that has to go into the XML structure, and less on the XML structure itself.


Repositories: 
 * [github.com/markkorput/xsd-populator](https://github.com/markkorput/xsd-populator). 
 * [https://github.com/markkorput/data-provider](https://github.com/markkorput/data-provider)


Producing XML is as simple as providing an XSD file and a DataProvider to the XsdProducer:
```ruby
require 'xsd_populator'

data_provider = CustomDataProvider.new(:some => 'data')
reader = XsdPopulator.new(:xsd => 'ddex-ern-v36.xsd', :provider => data_provider)
reader.populated_xml # <xml>...</xml>
```

DataProviders can be compiled from modular units and 

```ruby
describe "Include additional and overload providers" do
  module AdditionalProvider
    include DataProvider::Base

    provider 'provider2' do
      '#2'
    end
  end

  module OverwriteProvider
    include DataProvider::Base

    provider 'provider1' do
      '#1 overwritten'
    end
  end

  class BaseProvider
    include DataProvider::Base

    provider 'provider1' do
      '#1'
    end

    add AdditionalProvider
    add OverwriteProvider
  end

  it "lets you include modules with additional providers" do
    expect(BaseProvider.new.take('provider2')).to eq '#2'
  end

  it "lets you include modules that overload providers" do
    expect(BaseProvider.new.take('provider1')).to eq '#1 overwritten'
  end
end
```