

// https://github.com/markkorput/ciCMS/blob/master/src/ciCMS/cfg/ctree/TreeBuilder.hpp

// The TreeBuiler class uses the general purpose Builder class and adds
// hierarchical tree structure based on the Node class.

class TreeBuilder : public ::cms::cfg::Builder<Node> {

    //
    // ...
    //

    // addCfgObjectInstantiator lets the caller register a new component class that has its own 'cfg' method
    // After registering a component class, instances of the class can be instantiated
    // from configuration data that reference the specified name in the configuration.
    template<typename T>
    void addCfgObjectInstantiator(const string& name) {
      // Create builder (parent class) instantiator
      this->addInstantiator(name, [this, &name](CfgData& data){
        // create a named node for in the hierarchy structure, with an
        // instance of the specified type attached to it
        auto node = Node::create<T>(this->getName(data));

        // get the attached object from the node
        auto object = node->template getObject<T>();

        // "configure" the object by calling its cfg method
        this->configurator->apply(data, [this, object](ModelBase& mod){
          object->cfg(this->configurator->getCfg()->withData(mod.attributes()));
        });

        // notify observers
        BuildArgs args(node, object, &data);
        buildSignal.emit(args);

        this->notifyNewObject(object, data);

        // return result
        return node;
      });
    }

    //
    // ...
    //

};


// https://github.com/markkorput/ciCMS/blob/master/src/ciCMS/cfg/ctree/Node.h

// This Node class is a single item in a hierarchical structure and has an
// instance of any kind of class as payload. The create function shows how custom classes
// are instantiated together with the instance of the Node (in conscutive blocks of memory)
// and how destroy logic is recorded a proper cleanup, as it will be impossible to deduce the
// type (and thuse the destructor logic) from the payload at a later stage.

class Node : public ::ctree::Node {

  //
  // ...
  //

public:

  template<typename T>
  static Node* create(const std::string& name){
    // allocate Node and Object in single block of memory
    // this way we can always calculate the Node's address from
    // the corresponding Object's address.
    void* mem = std::malloc(sizeof(Node) + sizeof(T));
    T* objaddr = (T*)((uintptr_t)mem + sizeof(Node));
    // call the constructors of the Node and Object, but
    // assign memory address manually
    T* obj = new (objaddr) T();

    Node* n = new (mem) Node(name);

    // std::cout << "CREATED NODE with name '" << name << "' " << obj << std::endl;

    // register a destroyFunc, to be executed when n->destroy is called
    // this way the caller does not need to know about the type of object
    // that the node points to, but it will still do the proper destruction
    n->destroyFunc = [mem, obj, n](){
      // std::cout << "DELETING NODE with name: '" <<n->getName() << "': " << obj << std::endl;
      obj->~T();
      n->~Node();
      std::free(mem);
    };

    return n;
  }

  void destroy() {
    if (this->destroyFunc) {
      this->destroyFunc();
    } else {
      std::cerr << "no destroyFunc set on Node with name: " << name << std::endl;
    }
  }

  template<typename T>
  T* getObject(){ return (T*)((uintptr_t)this + sizeof(Node)); }

  void* getObjectPointer(){ return (void*)((uintptr_t)this + sizeof(Node)); }

  //
  // ...
  //
};